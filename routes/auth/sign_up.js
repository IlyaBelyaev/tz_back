const User = require('../../db/User');
const mongoose = require('mongoose');
const log = require('../../libs/log')(module);
const fs = require('fs');

module.exports = async function (req, res) {
    // Проверка на ввод всех данных пользователем
    try {
        const name = User.checkName(req.body.name);
        const email = User.checkEmail(req.body.email);
        const password = User.checkPassword(req.body.password);


        if (await name && await password && await email) {
            try {
                const newUser = new User({
                    name: req.body.name,
                    password: req.body.password,
                    email: req.body.email,
                });

                await newUser.save();
                res.header('Content-Type', 'text/plain');
                res.status(200).send('OK');

            } catch (err) {
                log.error(err);
                res.status(500).send('Ошибка сохранения');
            }
        }
    } catch (err) {
        res.status(400).send(err.message);
    }
};