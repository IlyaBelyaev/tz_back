const User = require('../../db/User');
const jwt = require('jsonwebtoken');
const config = require('../../libs/config');
const log = require('../../libs/log')(module);

module.exports = function (req, res) {
    User.findOne({email: req.body.email}).exec()
        .then(payload => {
            // Проверка на существование пользователя
            if (payload === null) {
                throw Error('Такая электронная почта не зарегистрирована!');
            } else

            // Проверка пароля
            if (payload.encryptPassword(req.body.password) !== payload.hashPassword) {
                throw Error('Неверный пароль!');
            } else
                return payload;
        })
        .then(payload => {
            // REFRESH TOKEN
            const refreshToken = jwt.sign({type: 'refresh'}, config.jwt.secretOrKey, { expiresIn: config.jwt.expiresInRefreshToken });
            User.update({ email: payload.email }, { $set: {refreshToken: refreshToken} }, (err) => {
                if (err) log.error(err);
            });

            // ACCESS TOKEN
            const tokenInfo = {
                _id: payload._id,
                name: payload.name,
                email: payload.email,
                cash: payload.cash,
                refreshToken: refreshToken
            };

            // Шифруем
            const token = jwt.sign(tokenInfo, config.jwt.secretOrKey, {expiresIn: config.jwt.expiresIn});
            res.header('Content-Type', 'application/json');
            res.status(200).send({ token: token });
        })
        .catch(err => {
            //console.log(err.message);
            res.header('Content-Type', 'text/plain');
            res.status(400).send(err.message);
        });

};