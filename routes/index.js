const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../libs/config');
const refreshToken = require('./auth/refresh_token');

const multer  = require('multer');
const upload = multer({ dest: 'public/img' });

module.exports = function (app) {
    app.post('/sign_up', require('./auth/sign_up')); // регистрация, принимает: почту(email), имя(name) и пароль(password)
    app.post('/sign_in', require('./auth/sign_in')); // вход, принимает: почту(email) и пароль(password)

    app.get('/get_lots', require('./items/get_lots')); // Получить массив с машинами на продаже

    app.post('/buy_car', verify, require('./items/buy_car')); // Купить машину, принимает id машины(id_car), id пользователя(по кукам)
    app.post('/sell_car', verify, require('./items/sell_car')); // Продать свою машину, принимает id пользователя(по кукам), id машины(id_car), стоимость продаваемой машины(cost) и описание(description)
    app.post('/add_car', verify, upload.single('image'), require('./items/add_car')); // Добавить машину не из коллекции, принимает название машины (name), стоимость(cost), описание (description), id владельца (id_seller)

    app.get('/get_lot_by_id', require('./items/get_lot_by_id')); // Получить массив с данными о машине по параметру id
    app.get('/get_user_balance', require('./user/get_user_balance')); // Получить баланс юзера по параметру id

    app.get('/get_bets_by_id', require('./items/get_bets_by_id')); // Получить ставки по id товара
    app.get('/get_user_cars', require('./user/get_user_cars')); // Получить машины принадлежащие юзеру

    app.get('/image',require('./get_image'));
};

function verify(req, res, next){
    try{
        const _refreshToken_ = jwt.decode(req.cookies.token).refreshToken;
        jwt.verify(req.cookies.token, config.jwt.secretOrKey, function (err, decoded) {
            if (decoded !== undefined){
                next()
            } else if (_refreshToken_) {
                refreshToken(_refreshToken_)
                    .then(newToken => {
                        res.cookie('token', newToken);
                    })
                    .then(result => next())
                    .catch(err => res.status(401).send('Unauth'));

            } else if (err) {
                res.status(401).send('Unauth');
            }
        });
    } catch (err) {
        if (err.message === "Cannot read property 'refreshToken' of null"){
            res.status(401).send('Вы не авторизированны');
        } else {
            console.log(err);
        }
    }
}