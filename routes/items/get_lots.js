const Lots = require('../../db/Lots');

module.exports = function (req, res) {
    const count = req.body.count;
    Lots.find({}, {}, {sort: '-date', limit: count})
        .then(result => res.send(result))
        .catch(err => {
            res.header('Content-Type', 'text/plain');
            res.status(400).send(err.message)
        });
};