const fs = require('fs');
//const path = require('path');

module.exports = function (req, res) {
    try{
        //const img = fs.readFileSync(path.join(__dirname, './public/img/'+req.query.id));
        const img = fs.readFileSync('./public/img/' + req.query.id);
        res.header('Content-Type', 'image/jpeg');
        res.end(img);
    } catch(err) {
        res.header('Content-Type', 'text/plain');
        res.status(400).send('Not found')
    }
};