const mongoose = require('mongoose');

const lotsSchema = mongoose.Schema({
    _id:  mongoose.Schema.Types.ObjectId,
    owner_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    car_name: {
        type: String,
        required: true
    },
    cost: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    car_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    bets: {
        type: Array,
        default: []
    },
    endTime: String,
    image: {
        type: String,
        required: true
    }
},{
    collection: "Lots",
    versionKey: false
});

lotsSchema.pre('save', function(next) {
    this._id = new mongoose.Types.ObjectId();

    next();
});

lotsSchema.pre('update', function(next) {
    this.endTime = new Date.now();

    next();
});

module.exports = mongoose.model('Lots', lotsSchema);